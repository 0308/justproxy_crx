const webpack = require('webpack');
const UglifyJSPlugin = require('uglifyjs-webpack-plugin')

let options = require('./webpack.config.js')

// node_modules中有些库需要编译，但是又不能让他编译babel自己，所以定向exclude这些
options.module.rules[0].exclude = [
  /node_modules\/babel-/m,
  /node_modules\/core-js\//m,
]

options.plugins = options.plugins.concat(
  new webpack.LoaderOptionsPlugin({
    minimize: true,
    debug: false
  }),
  new UglifyJSPlugin({
    uglifyOptions: {
      beautify: false,
      ecma: 6,
      compress: true,
      comments: false
    }
  })
)
module.exports = options