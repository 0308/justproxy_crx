const {execSync} = require('child_process')

const del = require('del')
const makeDir = require('make-dir')
const gulp = require('gulp')
const replace = require('gulp-replace')
const zip = require('gulp-zip')

const packageJson = require('./package.json')
const manifestJson = require('./manifest.json')


async function compile() {
  del.sync('./app/dist')

  let ret = execSync('npx webpack --config=webpack.config.prod.js')
  console.log(ret.toString())
}

async function collectBase() {
  del.sync('./build/collected')
  makeDir.sync('./build/collected')

  // 复制html文件，并将js的url从devServer的改成相对路径
  await new Promise(resolve => gulp
    .src([
      "./app/background.html",
      "./app/popup.html",
    ], {base: '.'})
    .pipe(replace('http://localhost:18081/', "./"))
    .pipe(gulp.dest('./build/collected'))
    .on('end', resolve)
  )

  // 复制资源文件
  await new Promise(resolve => gulp
    .src([
      "./app/static/**",
    ], {base: '.'})
    .pipe(gulp.dest('./build/collected'))
    .on('end', resolve)
  )

  // 复制编译后的前端文件
  await new Promise(resolve => gulp
    .src([
      './app/dist/*'
    ], {base: './app/dist/'})
    .pipe(gulp.dest('./build/collected/app/dist'))
    .on('end', resolve)
  )
}

async function collectCRX() {
  await new Promise(resolve => gulp
    .src(["./manifest.json"], {base: '.'})
    .pipe(gulp.dest('./build/collected'))
    .on('end', resolve)
  )
}


async function archiveCRX() {
  makeDir.sync('./build/archives')

  return new Promise(resolve => gulp
    .src(`./build/collected/**`, {base: `./build/collected/`})
    .pipe(zip(`${packageJson.name}_${manifestJson.version}_crx.zip`))
    .pipe(gulp.dest('./build/archives'))
    .on('end', resolve)
  )
}


gulp.task('compile', compile)

gulp.task('collect:base', collectBase)
gulp.task('collect:crx', collectCRX)

gulp.task('archive:crx', archiveCRX)

// compound tasks
gulp.task('release:crx', gulp.series('compile', 'collect:base', 'collect:crx', 'archive:crx'))

gulp.task('default', gulp.series('release:crx'))