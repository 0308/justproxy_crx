const path = require('path')

const {optimize: {CommonsChunkPlugin}, DefinePlugin} = require('webpack')

const hostname = 'localhost';
const port = '18081';

function isDebug() {
  return process.env.NODE_ENV === 'development'
}

function appPath() {
  let slice = [].slice;
  let items;
  items = 1 <= arguments.length ? slice.call(arguments, 0) : [];
  return path.resolve.apply(path, [__dirname, 'app'].concat(slice.call(items)));
}

let options = {
  devtool: isDebug() ? 'eval-source-map' : '',  // 调试时，能看到未转化的源代码文件并且对应到相应的行数
  entry: {
    background: [appPath('js', 'background.js')],
    popup: appPath('js', 'popup.js'),
  },
  output: {
    path: appPath('dist'),
    publicPath: "http://localhost:18081/dist/",
    filename: '[name].js'
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loader: 'babel-loader',
        query: {
          presets: ['env'],
          plugins: ['transform-runtime']
        }
      },
      {
        test: /\.vue$/,
        loader: 'vue-loader'
      },
      {
        test: /\.css/,
        exclude: /node_modules/,
        loaders: ['style-loader', 'css-loader']
      },
      {
        test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        loader: "url-loader?limit=10000&minetype=application/font-woff"
      }, {
        test: /\.(ttf|eot|svg|png)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        loader: "file-loader"
      }, {
        test: /\.json/,
        loader: 'json-loader'
      }
    ]
  },
  resolve: {
    extensions: ['.js', '.vue'],
    alias: {
      'vue$': 'vue/dist/vue.js',
    }
  },
  plugins: [
    new DefinePlugin({
      __DEBUG: isDebug()
    }),
    new CommonsChunkPlugin({
      name: "commons",
      minChunks: 2
    }),
  ],
  devServer: {
    host: hostname,
    port: port
  }
};

module.exports = options;