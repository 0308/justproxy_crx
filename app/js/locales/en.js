export default {
  product: 'Just Proxy VPN',
  service: {
    toggle_tooltip: "Toggle Proxy",
    hotkey_win: "Alt+J",
    hotkey_mac: "Option+J",
    refresh_tooltip: 'Refresh proxy status',
    register_remind: {
      message: 'Sign up now to ensure you can reinstall without worrying about membership.',
      button: 'Complete your account',
    },
    not_in_service: "your subscription is expired",
    go_premium: "Top up now for ONLY $1.99 a year",
    profile: {
      get_premium: 'Get Premium',
      upgrade: 'upgrade',
      early_adopter: "Early adopter, no need to pay to upgrade!",
      save_account: 'Save Account',
      dropdown: {
        more_app: 'Get JP for Android/iOS',
        login: 'Log in',
        logout: 'Log out',
        support: 'Support',
        top_up: 'Top Up',
        buy_service: 'Get Premium',
        register: 'Sign up',
      },
      common_modal: {
        yes: 'Yes',
        no: 'No',
        ok: 'OK',
      },
      support_modal: {
        title: 'Support',
        description: 'Describe the problem you are having in detail. We do open and read every email.',
        email: 'hello@justproxy.io'
      },
    },
  },
  location: {
    checking: 'checking',
    blocked: 'unreachable',
    premium: 'premium',
    free: 'free',
    basic: 'basic',
  },
  status: {
    visiting_from: 'You are visiting all websites from',
    off: 'Just Proxy VPN is currently off.'
  },
  conflict: {
    found: 'Just Proxy VPN cannot work while following extension(s) has control of your proxy settings.',
    resolve: 'Click to disable conflicting extension(s)'
  },
  rate: {
    works: {
      question: 'Does it work ?',
      yes: 'Yes, it works!',
      no: 'No, not really'
    },
    works_yes: {
      question: 'How about a rating on the Chrome Webstore, then?',
      yes: 'Ok, sure',
      no: 'No, thanks'
    },
    works_no: {
      question: 'Mind giving us some feedback ?',
      yes: 'Ok, sure',
      no: 'No, thanks'
    }
  },
  news: {
    android: {
      title: "Also available on Android!",
      approve: "Get it",
      deny: "Not interested",
    }
  },
  register: {
    header: 'Just Proxy VPN',
    login_link: "Login",
    message: 'sign up now to get 1 hour free subscription<br>no payment required',
    email: {
      label: 'Email',
      placeholder: 'Email'
    },
    password: {
      label: 'Password',
      placeholder: 'Password (min 6 characters)'
    },
    submit_button: 'Sign up'
  },
  login: {
    header: 'Just Proxy VPN',
    register_link: 'Sign up',
    email: {
      label: 'Email',
      placeholder: 'Email'
    },
    password: {
      label: 'Password',
      placeholder: 'Password (minimum of 6 characters)'
    },
    submit_button: 'Log in',
    reset_link: 'Forget your password?'
  },

  reset: {
    header: 'Reset your password',
    email: {
      label: 'Email',
      placeholder: 'Email'
    },
    send_button: 'Reset password',
    sent_message: 'An email with a reset code was sent to <b>{email}</b>',
    code: {
      label: 'Code',
      placeholder: 'Enter and verify the reset code'
    },
    password: {
      label: 'New password',
      placeholder: 'new password'
    },
    confirm_button: 'Reset',
    success_message: 'password updated successfully'
  },
  confirm: {
    header: 'Verify your email address',
    message: {
      header: 'One last step',
      body: 'Please enter 6 digits confirmation code we sent to <b>{email}</b>'
    },
    code: {
      label: 'Confirmation code',
      placeholder: 'Enter confirmation code'
    },
    submit_button: 'Submit',
    wrong_email: 'Not my email?',
    re_sign_up: 'Sign up again'
  },
  resender: {
    resend: 'Resend',
    sending: 'sending ...',
    done: 'sent',
    fail: 'unable to resend email, please try again later'
  },
  buy: {
    heading: 'Upgrade to Premium',
    sub_heading: 'for Chrome Extension',
    more_locations: 'Access to <b>{count}</b> VPN Locations',
    high_speed: 'Higher Speed & Lower Latency',
    android: 'Android Premium Membership',
    money_back: '14 Days Money Back Guarantee',
    cancel_anytime: 'Cancel Anytime',
    monthly: '$5/month',
    yearly: '$50/year',
    order: 'Order Now',
    buy_basic: 'Buy @ ${price} / year',
    pay_with_android: 'Pay with Android',
    go_back: 'go back',
  },
  errors: {
    format: {
      EMAIL_REQUIRED: 'Please enter your email',
      EMAIL_INVALID: 'Please enter a valid email address',
      PASSWORD_REQUIRED: 'Please enter your password',
      PASSWORD_MIN_LENGTH: 'Minimum of 6 characters',
      RESET_CODE_REQUIRED: 'Please enter confirmation code',
      CONFIRM_CODE_REQUIRED: 'Please enter confirmation code'
    },
    api: {
      timeout: 'server error - please try again later',
      server: 'server error - please try again later',
      parse: 'server error - please try again later',
      not_authenticated: 'Please login first',
      taken: 'The email address is already signed up',
      email_not_found: 'The email address is not yet signed up',
      invalid_password: "The email and password you entered don't match.",
      email_confirmed: 'The email address is already confirmed',
      invalid_confirmation_code: 'Invalid confirmation code',
      invalid_reset_code: 'Invalid reset code',
      reset_code_expired: 'Expired reset code'
    }
  },
  notifications: {
    ack_subscription: {
      title: 'Thank you for choosing Just Proxy VPN',
      message: 'You have unlocked all Premium features'
    },
    ack_payment: {
      title: 'Your purchase was successful',
      message: 'Thank you for choosing Just Proxy VPN.'
    },
    service_expired: {
      title: 'Subscription expired',
      message: 'Your Just Proxy VPN subscription is expired'
    }
  }
}