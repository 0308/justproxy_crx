import WebAPI from '../modules/WebAPI';
import _ from 'lodash'

const assert = require('chai').assert;

let newUserSessionId = '05ac826a0aaf4f52b2f92197622861b305d977f4-session.account.id=574845a20d00000f00256851';
let registeredSessionId = '4ea2f3c96200bec6daeacebe9e4701afc465c8e5-session.account.id=574847c10d0000ac0825789e'


describe('WebAPI', () => {
  it('fetchProxyList & event', async () => {
    WebAPI.client.setServers([
      'http://lakdjflkajdlfkjaljkflajdf.com',
      // 'https://www.justproxy.io',
      'https://mazed2ef.me'
    ])

    let ret = await WebAPI.fetchProxyList()
    assert.isOk(ret.length);
  })

  xit('newUser', function (done) {
    WebAPI._newUser()
          .then(ret => {
            assert.isString(ret.sessionId)
            assert(ret.success)
            console.log(ret);
            done();
          })
  })

  xit('register', function (done) {
    WebAPI._register(`luwenjin+${new Date().getTime()}@gmail.com`, '123456')
          .then(ret => {
            console.log(ret)
            done()
          })
  })

  xit('confirmEmail', function (done) {
    WebAPI._register(`luwenjin+${new Date().getTime()}@gmail.com`, '123456')
          .then(ret => {
            let sessionId = ret.sessionId;

            WebAPI._fetchProfile(sessionId).done(ret => {
              let confirmCode = ret.payload.confirmCode;

              WebAPI.confirmEmail(confirmCode, sessionId).done(ret => {
                assert(ret.success)

                WebAPI._fetchProfile(sessionId).done(ret => {
                  assert(ret.payload.confirmed === true);
                  done()
                })
              })
            })
          })
  })

  xit('login', function (done) {
    let account = 'luwenjin+1464354992041@gmail.com';
    let password = '123456';
    WebAPI._login(account, password)
          .then(ret => {
            assert(ret.success);
            assert(ret.sessionId);
            done()
          })
  })

  xit('fetchProfile(new user)', function (done) {
    WebAPI._fetchProfile(newUserSessionId)
          .then(ret => {
            assert(ret.success);
            assert.sameMembers(_.keys(ret.payload), ['accountId', 'serviceDue', 'status'])
            assert(ret.payload.accountId);
            assert(ret.payload.serviceDue == 0);
            assert(ret.payload.status == 'NEW');
            console.log(ret)
            done()
          })
  })

  xit('fetchProfile(registered user)', function (done) {
    WebAPI._fetchProfile(registeredSessionId)
          .then(ret => {
            console.log(ret);
            assert(ret.success);
            assert.sameMembers(_.keys(ret.payload),
              ['accountId', 'serviceDue', 'status', 'confirmCode', 'confirmed', 'email']
            )
            done()
          })
  })

  xit('requestCancel', function (done) {
    WebAPI._requestCancel(registeredSessionId)
          .then(ret => {
            console.log(ret);
            done()
          })
  })

  xit('requestRefund)', function (done) {
    WebAPI._requestRefund(registeredSessionId)
          .then(ret => {
            console.log(ret);
            done()
          })
  })
})