import {Client, EVENTS} from '../modules/AjaxClient';
import _ from 'lodash'

const assert = require('chai').assert;


describe('AjaxClient', function () {
  this.timeout(5000)

  it('ajax & event', async () => {
    let client = new Client()
    let eventSuccess, eventUrl

    client.on(EVENTS.DONE, (success, url) => {
      eventSuccess = success
      eventUrl = url
    })

    let ret = await client.ajax({
      url: 'http://httpbin.org/get',
    })
    assert.isOk(ret)
    assert.isTrue(eventSuccess);
    assert.isOk(eventUrl);
  })

  xit('timeout', async () => {
    let client = new Client()
    let eventFail, eventUrl

    client.on(EVENTS.DONE, (success, url, _) => {
      eventFail = !success
      eventUrl = url
    })
    client.setTimeout(2000)

    let ret = await client.ajax({
      url: 'http://httpbin.org/delay/2',
    })

    // assert.isOk(ret)
    assert.equal(ret.error, 'timeout')
    assert.isTrue(eventFail);
    assert.isOk(eventUrl);
  })

  it('robustAjax', async () => {
    let client = new Client()
    client.setServers([
      'http://aldfjkalsdjfkajdsflakjdf.com',
      'http://httpbin.org',
    ])

    let ret = await client.robustAjax({
      url: 'http://aldfjkasdlfkjaldskfjlaksdjflkajsdf.com/get',
    })
    assert.equal(ret.url, 'http://httpbin.org/get')

    assert.deepEqual(client.getBestServers(), [
      'http://httpbin.org',
      'http://aldfjkalsdjfkajdsflakjdf.com',
    ])
  })
})