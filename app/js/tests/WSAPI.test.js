import chai from 'chai'
import WSAPI from '../modules/WSAPI';
let assert = chai.assert;

const URL = 'wss://www.justproxy.io/ws'
WSAPI.initInBackground(URL);

describe('WSAPI', function () {
  this.timeout(3000);
  it('should emit "open" message when connected', function (done) {
    WSAPI.removeAllListeners();
    WSAPI.onOpen(() => {
      done()
    })
  });

  it('should emit "close" message when close', done => {
    WSAPI.removeAllListeners();
    WSAPI.onClose(() => {
      done()
    })
    WSAPI.close();
  })


  it('should not  emit "close" again, if already in closed state', function (done) {
    this.timeout(1000)
    WSAPI.removeAllListeners();
    WSAPI.onClose(() => {
      assert.fail('no close again')
    })
    WSAPI.close();
    setTimeout(() => done(), 900);
  })

  it('can connect again after close', done => {
    WSAPI.removeAllListeners();
    WSAPI.open()
    WSAPI.onOpen(() => {
      done();
    })
  })

  it('can send and receive message', done => {
    WSAPI.removeAllListeners();
    WSAPI.onMessage((m) => {
      console.log(m)
      done()
    })
    WSAPI.send('hello', 'world');
  })

});