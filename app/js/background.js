import ConflictManager from './modules/ConflictManager.js'
import ProxyManager from './modules/ProxyManager.js'
import WebAPI from './modules/WebAPI.js';
import WSAPI from './modules/WSAPI.js';
import User from './modules/User.js';
import settings from './settings.js'
import locales from './locales/index.js'
import './modules/analytics.js'

const debug = require('debug')('background');

const COLOR_ICON = {
  path: {
    19: "/app/static/img/icon-color-19.png",
    38: "/app/static/img/icon-color-38.png"
  }
};
const GREY_ICON = {
  path: {
    19: "/app/static/img/icon-grey-19.png",
    38: "/app/static/img/icon-grey-38.png"
  }
};

function init() {
  // 设置卸载url
  chrome.runtime.setUninstallURL(WebAPI.getUninstallUrl());

  ConflictManager.backgroundInit()
  ProxyManager.backgroundInit()

  WSAPI.initInBackground('wss://www.justproxy.io/ws')
  User.sessionId.watch((newSession, oldSession) => {
    newSession ? WSAPI.sendSetProperty({'session': newSession}) : WSAPI.sendUnsetProperty('session');
  })
  WSAPI.onOpen(() => {
    User.sessionId.get() && WSAPI.sendSetProperty({'session': User.sessionId.get()});
  })
  WSAPI.onMessage((type, payload) => {
    switch (type) {
      case WSAPI.REFRESH_PROFILE:
        User.pullProfile();
        break;

      case WSAPI.ACK_SUBSCRIPTION:
        User.pullProfile();
        chrome.notifications.create(new Date().getTime().toString(), {
          type: 'basic',
          iconUrl: '/app/static/img/icon-color-38.png',
          title: locales.en.notifications.ack_subscription.title,
          message: locales.en.notifications.ack_subscription.message,
        })
        break;

      case WSAPI.ACK_PAYMENT:
        User.pullProfile();
        if ((User.profilePV.get() || {}).status === 'ACTIVE') return;

        chrome.notifications.create(new Date().getTime().toString(), {
          type: 'basic',
          iconUrl: '/app/static/img/icon-color-38.png',
          title: locales.en.notifications.ack_payment.title,
          message: locales.en.notifications.ack_payment.message,
        })
        break;
    }
  })

  // 发送初次运行统计
  if (!localStorage['_firstRun']) {
    // ga('send', 'pageview', '/crx/first-run');
    localStorage['_firstRun'] = (new Date()).getTime();
  }

  // 发送在线统计
  // let version = chrome.runtime.getManifest().version
  // ga('send', 'pageview', `/crx/background/${version}`)
  // setInterval(() => {
  //   ga('send', 'pageview', `/crx/background/${version}`)
  // }, 1000 * 3600 * 24);  // 24小时后重发

  // 获得用户session_id
  if (User.sessionId.get()) {
    User.pullProfile();
  }

  // 拉取代理配置
  if (!localStorage.getItem('no-pull')) {
    ProxyManager.pullProxyList();
    setInterval(() => ProxyManager.pullProxyList(), 6 * 3600 * 1000); // 6小时重拉一次
  }

  // 根据代理模式修改图标颜色
  function updateBadgeIcon() {
    let icon
    if (ProxyManager.mode.get() !== 'on' || !User.inService() || !User.hasSession()) {
      icon = GREY_ICON
    } else {
      icon = COLOR_ICON
    }
    try {
      chrome.browserAction.setIcon(icon);
    } catch (e) {
    }
  }

  ProxyManager.mode.watch(updateBadgeIcon, true)

  User.on(User.PROFILE_CHANGED, updateBadgeIcon)
  User.on(User.IN_SERVICE_CHANGED, inService => {
    updateBadgeIcon()

    if (!inService) {
      chrome.notifications.create(new Date().getTime().toString(), {
        type: 'basic',
        iconUrl: '/app/static/img/icon-color-38.png',
        title: locales.en.notifications.service_expired.title,
        message: locales.en.notifications.service_expired.message,
      })
    }
  })

  // 根据插件冲突情况,显示感叹号
  chrome.browserAction.setBadgeText({text: ""});
  ConflictManager.conflicts.watch(exts => {
    chrome.browserAction.setBadgeText({text: exts.length ? '!' : ''});
  }, true);

  // 快捷键
  chrome.commands.onCommand.addListener((command) => {
    switch (command) {
      case "toggle-proxy":
        if (User.inService()) {
          ProxyManager.mode.set(ProxyManager.mode.get() === 'on' ? 'off' : 'on');
        } else {
          chrome.notifications.create(new Date().getTime().toString(), {
            type: 'basic',
            iconUrl: '/app/static/img/icon-color-38.png',
            title: locales.en.notifications.service_expired.title,
            message: locales.en.notifications.service_expired.message.replace('{price}', User.price()),
          })
        }
    }
  })

  // 支付后第二次启动时显示打分界面
  if (User.inService()) {
    settings.launchCountAfterPay.set(settings.launchCountAfterPay.get() + 1);
    if (settings.launchCountAfterPay.get() > 1 && settings.showRateContent.get() === 'wait') {
      settings.showRateContent.set('yes');
    }
  }

  // 检测server是否可用
  function checkServerStatus() {
    if (!navigator.onLine) return;

    let forceTunnelRules = ProxyManager.forceTunnelRules.get() || [];
    if (forceTunnelRules.indexOf(settings.HOST) >= 0) return; // 已经将服务器加入代理，就不再检测了

    WebAPI.fetchProxyList()
          .then(data => {
            if (data.error) {
              ProxyManager.forceTunnelRules.set([settings.HOST])
            }
          })
  }

  function foreverCheckServerStatus() {
    checkServerStatus();
    setInterval(checkServerStatus, 10 * 60 * 1000);
  }

  if (navigator.onLine) {
    foreverCheckServerStatus();
  } else {
    window.addEventListener('online', function onOnline() {
      window.removeEventListener('online', onOnline)
      foreverCheckServerStatus()
    })
  }
}

init();

if (__DEBUG) {
  window.WebAPI = WebAPI;
  window.WSAPI = WSAPI;
  window.ProxyManager = ProxyManager;
}
