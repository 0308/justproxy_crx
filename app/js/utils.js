import some from 'lodash/some'
import values from 'lodash/values'


export function openUrl(url) {
  chrome.tabs.create({url: url});
}

function extractHost(url) {
  let pattern = /\w+:\/\/([^\/]+)/
  let result = pattern.exec(url)
  return !!result && result.length > 1 ? result[1] : ''
}

function naturalDuration(ms) {
  if (isNaN(ms)) {
    return ''
  }
  const units = ['毫秒', '秒', '分钟', '小时', '天', '年', '世纪']
  const multiplies = [1, 1000, 60, 60, 24, 365, 100]

  let n = ms
  let i = 0
  for (i = 0; i < units.length; i++) {
    n /= multiplies[i]
    if (n < 1) {
      n *= multiplies[i]
      i -= 1
      break
    }
  }
  i >= units.length ? i = units.length - 1 : ''

  return `${n.toFixed(1)}${units[i]}`
}

function hasAnyValue(obj) {
  return some(values(obj))
}

module.exports = {
  openUrl,
  extractHost,
  naturalDuration,
  hasAnyValue
}
