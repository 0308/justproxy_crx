import moment from 'moment'

let debug = require('debug')('UserMixin')

export default {
  computed: {
    isGuest() {
      return this.profile && !this.profile.email;
    },

    guestName() {
      return `Guest#${(this.profile.accountId || '').slice(-4)}`
    },

    inService() {
      return (this.profile || {}).serviceDue > Date.now();
    },

    serviceDueFromNow() {
      return moment(this.profile.serviceDue).fromNow(true)
    },

    serviceDueText() {
      return moment(this.profile.serviceDue).calendar();
    }
  }
}