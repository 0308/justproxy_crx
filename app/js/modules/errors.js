import locales from '../locales'

const messages = locales.en.errors.api;

function getMessage(ret) {
  return messages[ret.error] || ret.error
}

export default {
  // general error
  PARSE: 'parse',
  TIMEOUT: 'timeout',
  SERVER: 'server',

  // need sessionId but no valid one provided
  NOT_AUTHORIZED: 'not_authenticated',

  // register
  EMAIL_TAKEN: 'taken',

  // login
  EMAIL_NOT_FOUND: 'email_not_found',
  INVALID_PASSWORD: 'invalid_password',

  // confirm email
  ALREADY_CONFIRMED: "email_confirmed",
  INVALID_CONFIRMATION_CODE: 'invalid_confirmation_code',

  // request password reset
  // ERROR_EMAIL_NOT_FOUND

  // password reset
  INVALID_RESET_CODE: 'invalid_reset_code',
  RESET_CODE_EXPIRED: 'reset_code_expired',

  getMessage,
}