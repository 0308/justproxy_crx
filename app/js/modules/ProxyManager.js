import async from 'async'
import now from 'lodash/now'
import get from 'lodash/get'
import flatMap from 'lodash/flatMap'
import fill from 'lodash/fill'
import isFunction from 'lodash/isFunction'
import zipObject from 'lodash/zipObject'
import values from 'lodash/values'
import mean from 'lodash/mean'
import flatten from 'lodash/flatten'
import sortBy from 'lodash/sortBy'
import PersistValue from 'persist-value'
import appBus from 'app-event-bus/crx'
import WebAPI from './WebAPI';
import User from './User.js';
import settings from '../settings.js'

let debug = require('debug')('ProxyManager')

const MESSAGE_SPEED_TEST_START = "speed test start"
const MESSAGE_SPEED_TEST_FINISH = "speed test finish"

let mode = new PersistValue('proxyMode', 'off')

let selectedProxyId = new PersistValue('selectedProxyId', '')

let proxyList = new PersistValue('proxyList', require('../../config/proxies.json'));

let forceTunnelRules = new PersistValue('forceTunnelRules_20170113', []);
forceTunnelRules.set([])

const APP_VERSION = chrome.runtime.getManifest().version

let authHeader = btoa(JSON.stringify({
  "token": "it-is-not-cool-to-steal-traffic",
  "ver": APP_VERSION,
}))

let proxySpeeds = new PersistValue('proxySpeeds', {
  "testing": false,
  "regions": {
    /*
     REGION-ID: {
     testing: true/false,
     average: n, // -1 = block, >0 = elapsed, other = no data
     proxies: {
     PROXY-ADDR: elapsed  // >0 or -1
     }
     }
     */
  }
});

function getProxiesById(id) {
  let proxies = []
  proxyList.get().forEach((region) => {
    if (region.id === id) {
      proxies = region.proxies.filter((p) => p.up)
    }
  })
  return proxies
}

let hooked = false;

function hook(on) {
  if (hooked && !on) {
    hooked = false
    chrome.webRequest.onAuthRequired.removeListener(handleAuth)
    chrome.privacy.network.webRTCIPHandlingPolicy && chrome.privacy.network.webRTCIPHandlingPolicy.set({
      value: "default"
    })
  } else if (!hooked && on) {
    hooked = true
    chrome.webRequest.onAuthRequired.addListener(handleAuth, {urls: ["<all_urls>"]}, ["asyncBlocking"])
    chrome.privacy.network.webRTCIPHandlingPolicy && chrome.privacy.network.webRTCIPHandlingPolicy.set({
      value: "disable_non_proxied_udp"
    });
  }
}

let skipUpdatePAC = new PersistValue('skipUpdatePAC')

function activateForAPI() {
  skipUpdatePAC.set(true)

  let allProxies = flatMap(proxyList.get(), region => region.proxies)
  allProxies = fuzzySortProxies(allProxies)

  let includeHosts = [
    settings.HOST,
    ...settings.FALLBACK_HOSTS,
  ]
  return setPAC(allProxies, includeHosts, ['localhost'])
}

function deactivateForAPI() {
  skipUpdatePAC.set(false)
}

async function activateFor(f) {
  let ret
  await activateForAPI()
  if (f instanceof Promise) {
    ret = await f
  } else if (isFunction(f)) {
    ret = await f()
  } else {
    ret = f
  }
  await deactivateForAPI()
  return ret
}


function updatePAC() {
  if (skipUpdatePAC.get()) return;

  let id = getProxyId()
  let proxies = getProxiesById(id)

  let modeOn = mode.get() === 'on';
  if (modeOn) {
    if (proxies.length === 0) {
      modeOn = false;
    } else if (!User.hasSession() || !User.inService()) {
      modeOn = false;
    }
  }

  let bestProxies = fuzzySortProxies(proxies, 9999)
  if (!bestProxies || !bestProxies.length) {
    bestProxies = fuzzySortProxies(proxies, 99999);
  }
  if (!bestProxies || !bestProxies.length) {
    bestProxies = fuzzySortProxies(proxies, 999999);
  }
  if (bestProxies && bestProxies.length > 5) {
    bestProxies = bestProxies.slice(0, 5)
  }

  let includeHosts = []
  let excludeHosts = [
    settings.HOST.replace('www.', '*.'),
    '104.238.173.73',
    'localhost*',
    '127.0.0.1*',
    '192.168.*',
    '172.16.*', '172.17.*', '172.18.*', '172.19.*', '172.20.*', '172.21.*',
    '172.22.*', '172.23.*', '172.24.*', '172.25.*', '172.26.*', '172.27.*',
    '172.28.*', '172.29.*', '172.30.*', '172.31.*',
    '10.*'
  ]

  if (modeOn) {
    includeHosts = includeHosts.concat(forceTunnelRules.get() || [])
    includeHosts.push(settings.HOST)
    hook(true)
    setPAC(bestProxies, includeHosts, excludeHosts, false)
  } else {
    if ((forceTunnelRules.get() || []).length) {
      includeHosts = includeHosts.concat(forceTunnelRules.get())
      excludeHosts = ['*']
      hook(true)
      setPAC(bestProxies, includeHosts, excludeHosts, false)
    } else {
      hook(false)
      unsetPAC()
    }
  }
}


function setPAC(proxies, includeHosts, excludeHosts, defaultDirect = false) {
  let httpsProxyString = proxies.map(p => `HTTPS ${p.host}:${p.tlsPort}`).join(';');
  let script = `
    var proxyString=${JSON.stringify(httpsProxyString)};
    var includeHosts = ${JSON.stringify(includeHosts)};
    var excludeHosts = ${JSON.stringify(excludeHosts)};
    
    function FindProxyForURL(url, host) {
        for (var i= 0 ; i < includeHosts.length; i ++ ) {
            if (shExpMatch(host, includeHosts[i])) {
                return proxyString;
            }
        }
        
        for (var i= 0 ; i < excludeHosts.length; i ++ ) {
            if (shExpMatch(host, excludeHosts[i])) {
                return "DIRECT";
            }
        }
        
        return ${defaultDirect ? '"DIRECT"' : 'proxyString'};
    }`;

  return new Promise(resolve => {
    chrome.proxy.settings.set({
      value: {
        mode: 'pac_script',
        pacScript: {
          data: script,
          mandatory: true
        }
      },
      scope: 'regular'
    }, resolve)
  })
}

function unsetPAC() {
  return new Promise(resolve => {
    chrome.proxy.settings.clear({
      scope: 'regular'
    }, resolve);
  })
}

function fuzzySortProxies(proxies, maxLatency = 9999) {
  let li = proxies.map((p) => {
    // speed is latency
    let speed = getProxySpeed(p.host, p.tlsPort);
    if (speed === 0) speed = 9999;
    if (speed < 0) speed = 99999;

    debug('fuzzySort', p.host, p.tlsPort, speed)
    if (speed > 0) {
      speed *= (1 + Math.random() * 0.3); // 用来模糊排序
    }
    debug('fuzzySort fuzzed', p.host, p.tlsPort, speed)
    return {'proxy': p, 'speed': speed}
  });
  li = sortBy(li, 'speed');
  li = li.filter(x => x.speed < maxLatency)
  let ret = li.map((x) => x.proxy);
  debug('fuzzySort result', ret)
  return ret;
}


function handleAuth(details, callback) {
  debug('in auth handler', details)
  if (details.isProxy && details.realm.toLowerCase().indexOf("justproxy") >= 0) {
    debug('in auth handler: handle')
    callback({
      authCredentials: {
        username: 'A single death is a tragedy',
        password: 'a million deaths is a statistic'
      }
    })
  } else {
    callback()
  }
}

function getDefaultProxyId() {
  let loc = proxyList.get().filter(locData => {
    return (locData.proxies || []).filter(x => x.up).length
  })[0]
  return loc ? loc.id : null;
}

function getProxyId() {
  let id = selectedProxyId.get()

  // 如果上次选中的地区是付费地区,而自己不是付费用户,就给默认地区
  if (id) {
    let loc = proxyList.get().filter(x => x.id === id)[0];
    if (loc) {
      return loc.id
    }
  }

  // 如果没有选择默认地区,或者地区中的代理不可用,就给默认地区
  if (!id || !getProxiesById(id).length) {
    return getDefaultProxyId()
  }

  return id
}

function setProxyId(id) {
  selectedProxyId.set(id)
}

function testProxy(proxy, callback) {
  let host = proxy.host;
  let port = proxy.tlsPort;

  let t = now();
  $.ajax({
    method: 'GET',
    url: `https://${host}:${port}/check`,
    timeout: 1000 * 10
  }).done((data, status) => {
    let elapse = (now() - t) / 4; // tcp handshake:1 rtt, ssl handshake:2 rtt, http:1 rtt
    debug(`test ${host}:${port}`, elapse)
    callback(null, elapse);
  }).fail((status) => {
    debug(`test ${host}:${port}`, -1)
    callback(null, -1);
  })
}

function testRegion(regionId, callback) {
  let proxies = getProxiesById(regionId);
  async.mapLimit(proxies, 3, testProxy, (err, results) => {
    let regionData = {
      testing: false,
      average: mean(results.filter((x) => x > 0)) || -1, // -1 | > 0
      proxies: zipObject(
        proxies.map((x) => `${x.host}:${x.tlsPort}`),
        results
      )
    };
    let data = proxySpeeds.get();
    data['regions'][regionId] = regionData;
    proxySpeeds.set(data);

    let reportData = results.map((elapse, i) => {
      let proxy = proxies[i];
      let host = proxy.host;
      let port = proxy.tlsPort;
      return {host, port, elapse}
    })

    callback(null, reportData);
  })
}

function doTestAll() {
  if (proxySpeeds.get().testing) return;

  let currentRegionId = getProxyId();
  let regionIds = proxyList.get()
                           .filter((region) => region.id !== currentRegionId)
                           .map((region) => region.id);
  regionIds.unshift(currentRegionId);

  proxySpeeds.set(Object.assign(proxySpeeds.get(), {
    testing: true,
    regions: zipObject(
      regionIds,
      fill(Array(regionIds.length), {testing: true})
    )
  }));

  async.mapLimit(regionIds, 2, testRegion, (err, reportsList) => {
    let reports = flatten(reportsList);
    debug('testAll result', err, reports);
    User.reportSpeedTest(reports);

    proxySpeeds.set(Object.assign(proxySpeeds.get(), {testing: false}));
    appBus.emit(MESSAGE_SPEED_TEST_FINISH);
  })
}

function getRegionSpeed(id) {  // -1 = error, 0 = no data, >0 = elapse
  return (proxySpeeds.get()['regions'][id] || {}).average || 0;
}

function getProxySpeed(host, tlsPort) {  // -1 = error, 0 = no data, >0 = elapse
  let addr = `${host}:${tlsPort}`;
  for (let region of values(proxySpeeds.get()['regions'])) {
    if ((region['proxies'] || {})[addr] !== undefined) {
      return region['proxies'][addr]
    }
  }
  return 0;
}

function testAll() {
  appBus.emit(MESSAGE_SPEED_TEST_START);
}

function pullProxyList(cb) {
  WebAPI.fetchProxyList()
        .then((data) => {
          if (!data.error) {
            proxyList.set(data);
          }
          testAll(); // 无论是否数据变更,都强行测速一次
          cb && cb(data);
        })
}

function backgroundInit() {
  proxySpeeds.set(Object.assign(proxySpeeds.get() || {}, {testing: false})); // 防止上次crash时值为true,造成永远无法test
  debug("proxySpeeds", proxySpeeds.get());

  proxyList.watch(() => {
    updatePAC();
    testAll();
  })
  forceTunnelRules.watch(updatePAC)
  mode.watch(updatePAC)
  selectedProxyId.watch(updatePAC)
  User.on(User.IN_SERVICE_CHANGED, updatePAC)
  User.on(User.PROFILE_CHANGED, updatePAC)
  skipUpdatePAC.watch(updatePAC)

  appBus.on(MESSAGE_SPEED_TEST_FINISH, updatePAC);
  appBus.on(MESSAGE_SPEED_TEST_START, doTestAll);

  updatePAC();

  chrome.webRequest.onBeforeSendHeaders.addListener(
    (details) => {
      let headers = details.requestHeaders;
      let profile = User.profilePV.get()
      headers.splice(headers.length - 1, 0, ...[
        {name: "x-jp-oiaheoihgae", value: authHeader},
        {name: "ri7rl5Ac1", value: get(profile, 'accountId')},
        {name: "ri7rl5Ac2", value: String(get(profile, 'serviceDue'))},
        {name: "ri7rl5Ac3", value: APP_VERSION},
        {name: "ri7rl5Ac4", value: get(profile, 'signature')},
      ])
      return {requestHeaders: headers};
    },
    {urls: ["<all_urls>"]},
    ['blocking', 'requestHeaders']
  );
}

export default {
  MESSAGE_SPEED_TEST_START,
  MESSAGE_SPEED_TEST_FINISH,

  activateFor,

  forceTunnelRules,
  proxyList,
  proxySpeeds,
  mode,
  setProxyId,
  getProxyId,
  getRegionSpeed,
  getProxySpeed,
  pullProxyList,
  backgroundInit
}