import URL from 'url-parse'
import get from 'lodash/get'
import settings from '../settings'
import {Client, EVENTS} from './AjaxClient.js'
import PersistValue from 'persist-value'
import "./analytics.js"

const ROOT_URL = `${settings.SSL ? 'https' : 'http'}://${settings.HOST}`

let serverStats = new PersistValue('serverStats', {})

let client = new Client()
client.serverStats = serverStats.get()

let hosts = [settings.HOST, ...settings.FALLBACK_HOSTS]
client.setServers(hosts.map(host => `${settings.SSL ? 'https' : 'http'}://${host}`))

let allFailReported = false
// 读取 & 持久化保存server健康度
client.on(EVENTS.DONE, (success, url) => {
  serverStats.set(client.serverStats)
  let serverHost = new URL(url).host

  ga('send', 'pageview', `/server-stats/${serverHost}/request`)
  if (!success) {
    ga('send', 'pageview', `/server-stats/${serverHost}/fail`)

    let failCount = hosts.filter(host => get(client.serverStats, [host, 'success']) === false).length
    if (failCount === hosts.length && !allFailReported) {
      allFailReported = true
      ga('send', 'pageview', `/server-fail/ALL`)
    }
  }
})

function getUrl(path) {
  return client.getBestServers()[0] + path
}

function getUninstallUrl() {
  return getUrl('/crx/uninstall')
}


function _getJson(path, {headers} = {}) {
  let settings = {
    method: 'GET',
    url: ROOT_URL + path,
    dataType: 'json'
  }
  if (headers) {
    settings.headers = headers
  }
  return client.robustAjax(settings)
}

function _postJson(path, {data, headers} = {}) {
  let settings = {
    method: 'POST',
    url: ROOT_URL + path,
    dataType: 'json'
  };
  if (data) {
    settings.contentType = 'application/json';
    settings.data = JSON.stringify(data);
  }
  if (headers) {
    settings.headers = headers
  }
  return client.robustAjax(settings)
}

function fetchProxyList() {
  /*
   code: String // 地区码,用来显示地区
   desc: String // 用来在列表中展示
   id: String  // 唯一标示一个机房的id
   tpe: 'FREE' / 'PREMIUM',
   proxies: [{
   - host: String,
   - tlsPort: Number,  !! use this
   - up: true/false,
   }, ...]
   */
  return _getJson('/proxy/status.json');
}

function newUser() {
  /*
   success: true
   sessionId: String,
   */
  return _getJson('/user/new')
}

function fetchProfile(sessionId) {
  /*
   success: true,
   payload: {
   - accountId: "571e10700e0000e537aea37a",
   - serviceDue: 0,
   - status: 'NEW'(用户注册但是没有付费) / 'ACTIVE'(注册已经付费) / 'CANCELLED'(注册已经取消) / 'EARLY_ADOPTER'
   ----------- 注册用户 -----------------
   - serviceStart: int,  # 最后一个订单完成的时间,用来计算是否在退款时间内
   - confirmCode: 777777,
   - confirmed: true/false,
   - email: "xxx@xxx.com",
   }
   */
  return _getJson('/user/profile', {headers: {'API-SESSION': sessionId}});
}

function register(email, password, sessionId = null) { // 会发确认邮件
  /*
   success: true,
   sessionId: String,
   */

  return _postJson('/user/signup', {
    data: {
      signUpEmail: email,
      signUpPassword: password
    },
    headers: sessionId ? {'API-SESSION': sessionId} : undefined,
  })
}

function registerWithoutConfirm(email, password, sessionId = null) { // 不会发确认邮件
  /*
   success: true,
   sessionId: String,
   */
  return _postJson('/user/signup_without_confirm', {
    data: {
      signUpEmail: email,
      signUpPassword: password
    },
    headers: sessionId ? {'API-SESSION': sessionId} : undefined
  })
}

function login(email, password) {
  /*
   success: true,
   sessionId: String,
   */
  return _postJson('/user/login', {data: {email, password}})
}

function resendEmailConfirmation(sessionId) {
  /*
   success:true
   */
  return _getJson('/user/resend_email_confirmation', {headers: {'API-SESSION': sessionId}})
}

function confirmEmail(code, sessionId) {
  /*
   success:true
   */
  return _postJson('/user/confirm_email', {
    data: {code},
    headers: {'API-SESSION': sessionId}
  })
}

function requestPasswordReset(email) {
  return _postJson('/user/request_password_reset', {data: {email}})
}

function resetPassword(email, code, password) {
  return _postJson('/user/reset_password', {data: {email, code, password}})
}

function reportSpeedTest(results, sessionId) { // [{host:, port:, elapse:}, ...]
  return _postJson('/analytics/speed_test', {
    data: results,
    headers: {
      "API-SESSION": sessionId || "",
    }
  });
}

function trackRegionSwitch(from, to, sessionId) {
  return _postJson(
    `/analytics/switch_region?from=${encodeURIComponent(from)}&to=${encodeURIComponent(to)}`,
    {headers: {'API-SESSION': sessionId}});
}

function trackServiceSwitch(on, sessionId) {
  return _postJson(
    `/analytics/switch_extension?status=${on}`,
    {headers: {'API-SESSION': sessionId}});
}

function requestCancel(sessionId) {
  return _getJson(
    `/user/request_cancel`,
    {headers: {'API-SESSION': sessionId}});
}

function requestRefund(sessionId) {
  return _getJson(
    `/user/request_refund`,
    {headers: {'API-SESSION': sessionId}});
}

function redirectUrl(path, sessionId) {
  return getUrl(`/redirect?to=${encodeURIComponent(path)}&session=${encodeURIComponent(sessionId)}`)
}

export default {
  client,

  getUninstallUrl,
  _getRedirectUrl: redirectUrl,

  fetchProxyList,
  requestPasswordReset,
  resetPassword,

  _newUser: newUser,
  _fetchProfile: fetchProfile,
  _register: register,
  _registerWithoutConfirm: registerWithoutConfirm,
  _login: login,
  _confirmEmail: confirmEmail,
  _resendEmailConfirmation: resendEmailConfirmation,
  _trackRegionSwitch: trackRegionSwitch,
  _trackServiceSwitch: trackServiceSwitch,
  _requestCancel: requestCancel,
  _requestRefund: requestRefund,
  _reportSpeedTest: reportSpeedTest,
}