import appBus from 'app-event-bus/crx'

const debug = require('debug')('WSAPI');

// states
const STATE_CONNECTING = 0;
const STATE_OPEN = 1;
const STATE_CLOSING = 2;
const STATE_CLOSED = 3;

// times
const PING_INTERVAL = 20 * 1000;
const RECONNECT_INTERVAL = 30 * 1000;

// message
const ON_CLOSED = 'WSAPI_CLOSED--1e321c7b-9ff5-4c2f-aaf0-ffa64451d897'
const ON_OPEN = 'WSAPI_OPEN--df9e0d2a-46f3-4583-9d8b-9112dee1cb9c'
const ON_MESSAGE = 'WSAPI_MESSAGE--4dcc1fb0-2e5c-4e9c-acde-6cde1af3c13a'
const DO_OPEN = 'WSAPI_DO_OPEN--396cdc64-b041-467b-bcec-2c6d4eb247ce'
const DO_SEND = 'WSAPI_DO_SEND--299078e5-94ea-4a1d-9640-9fa89cad8ca2'
const DO_CLOSE = 'WSAPI_DO_CLOSE--8900c6d4-8ed6-48ba-8183-ea8acbf03ba1'

function now() {
  return (new Date()).getTime();
}


class WSAPI {
  constructor(url) {
    this.url = url;
    this.ws = null;
    this.pingTimer = null;
    this.pongTime = null;
    this.manualClose = false;
  }

  pingAndCheck() {
    if (this.ws && this.ws.readyState === STATE_OPEN) {
      debug('.');
      this.ws.send(".");

      if (now() - this.pongTime > 3 * PING_INTERVAL) {
        debug('long time not heard from remote, close to reconnect');
        this._terminate();
      }
    }
  }

  connect() {
    this.cleanup();
    this.ws = new WebSocket(this.url);
    this.ws.onopen = (e) => this.onOpen(e);
    this.ws.onclose = (e) => this.onClose(e);
    this.ws.onmessage = (e) => this.onMessage(e);
    this.ws.onerror = (e) => this.onError(e);
    this.manualClose = false;
  }

  sendMessage(type, payload) {
    const data = {
      tpe: type,
      payload
    };
    if (this.ws && this.ws.readyState === STATE_OPEN) {
      debug('->', data);
      this.ws.send(JSON.stringify(data));
    } else {
      debug('!! not able to send:', data)
    }
  }

  close() {
    // 手动关闭连接
    this.manualClose = true;
    this._terminate();
  }

  _terminate() {
    if (this.ws) {
      this.ws.close();
      this.onClose();
    }
  }

  onOpen(e) {
    if (!e || e.target !== this.ws) return;
    debug('onOpen', e, this.ws.readyState);

    appBus.emit(ON_OPEN);

    this.pingTimer = setInterval(
      () => this.pingAndCheck(),
      PING_INTERVAL
    );
    this.pongTime = now()
  }

  onMessage(e) {
    if (!e || e.target !== this.ws) return;
    this.pongTime = now();

    if (e.data === ".") {
      return;
    }

    let msg = JSON.parse(e.data);
    debug('<-', msg);
    appBus.emit(ON_MESSAGE, msg.tpe, msg.payload);
  }

  onClose(e) {
    if (!e || e.target !== this.ws) return;
    debug('onClose', e);
    console.log('onClose', e.code, e.reason);

    this.cleanup();
    appBus.emit(ON_CLOSED);

    if (!this.manualClose) {  // 断开自动重连
      setTimeout(() => {
        if (!this.manualClose) this.connect();
      }, RECONNECT_INTERVAL);
      debug(`close unexpected, will retry in ${RECONNECT_INTERVAL / 1000} s`);
    }
  }

  onError(e) {
    console.log('onError', e);
  }

  cleanup() {
    if (this.ws) {
      this.ws.onopen = null;
      this.ws.onclose = null;
      this.ws.onmessage = null;
      this.ws.close();
      this.ws = null;
    }
    clearInterval(this.pingTimer);
    this.pingTimer = null;
    this.pongTime = null;
  }
}

let wsapi = null;

function initInBackground(url) {
  if (wsapi == null) {
    wsapi = new WSAPI(url);
    // wsapi.connect();

    appBus.on(DO_OPEN, () => {
      wsapi && !wsapi.ws && wsapi.connect()
    })
    appBus.on(DO_SEND, (type, payload) => {
      wsapi && wsapi.sendMessage(type, payload);
    });
    appBus.on(DO_CLOSE, () => {
      wsapi && wsapi.close()
    })
  }
}

function open() {
  appBus.emit(DO_OPEN);
}

function send(type, payload) {
  appBus.emit(DO_SEND, type, payload);
}

function close() {
  appBus.emit(DO_CLOSE);
}

function sendSetProperty(kvo) {
  send('SET_PROPERTY', kvo)
}

function sendUnsetProperty(...keys) {
  send('UNSET_PROPERTY', keys)
}

function onOpen(callback) {
  appBus.on(ON_OPEN, callback);
}

function onMessage(callback) { // callback(type, payload))
  appBus.on(ON_MESSAGE, callback)
}

function onClose(callback) {
  appBus.on(ON_CLOSED, callback)
}

function removeAllListeners() {
  appBus.off(ON_OPEN);
  appBus.off(ON_MESSAGE);
  appBus.off(ON_CLOSED);
}

export default {
  REFRESH_PROFILE: 'REFRESH_PROFILE', // payload: {}， 通知刷新profile
  ACK_SUBSCRIPTION: 'ACK_SUBSCRIPTION', // payload: {}， 订阅成功
  ACK_PAYMENT: 'ACK_PAYMENT', // payload: {}， 购买成功

  open,
  send,
  sendSetProperty,
  sendUnsetProperty,
  close,

  onOpen,
  onMessage,
  onClose,
  removeAllListeners,

  initInBackground
};
