import EventEmitter from 'eventemitter3'
import PersistValue from 'persist-value';
import WebAPI from './WebAPI';
import errors from './errors';
import utils from '../utils.js'

let debug = require('debug')('User')

let EE = new EventEmitter()

let sessionId = new PersistValue('sessionId');
let profilePV = new PersistValue('profile', {});

async function register(email, password) {
  let ret = await WebAPI._register(email, password, sessionId.get())
  if (ret.error) {
    return ret
  } else {
    sessionId.set(ret.sessionId)
    return await pullProfile()
  }
}

async function login(email, password) {
  let ret = await WebAPI._login(email, password)
  if (ret.error) {
    return ret
  } else {
    sessionId.set(ret.sessionId);
    return await pullProfile()
  }
}

async function pullProfile() {
  if (sessionId.get()) {
    let ret = await WebAPI._fetchProfile(sessionId.get())
    if (!ret.error) {
      profilePV.set(ret.payload);
    }
    return ret
  } else {
    return {error: errors.NOT_AUTHORIZED}
  }
}

function signOut() {
  profilePV.restore();
  sessionId.restore();
}

function isGuest() {
  return !profilePV.get('email');
}

function hasSession() {
  return !!sessionId.get()
}

function inService() {
  return profilePV.get('serviceDue') > new Date().getTime()
}

function redirectUrl(path) {
  return WebAPI._getRedirectUrl(path, sessionId.get())
}

function openPayPage() {
  let url = redirectUrl('/payment/paypal/one_year_basix') // 1.99
  utils.openUrl(url);
}

function price() {
  return 1.99
}

const IN_SERVICE_CHANGED = 'IN_SERVICE_CHANGED'
const PROFILE_CHANGED = 'PROFILE_CHANGED'

let prevInService = inService();
setInterval(() => {
  if (inService() !== prevInService) {
    if (hasSession()) {
      EE.emit(IN_SERVICE_CHANGED, inService())
    }
  }
  prevInService = inService();
}, 1000)

profilePV.watch((nv, ov) => {
  EE.emit(PROFILE_CHANGED, nv, ov)
})

export default {
  IN_SERVICE_CHANGED,
  PROFILE_CHANGED,

  profilePV,
  sessionId,

  on: (...args) => EE.on(...args),

  register: (email, password) => register(email, password), // => with profile payload
  login: (email, password) => login(email, password), // => with profile payload
  pullProfile: pullProfile,
  confirmEmail: code => WebAPI._confirmEmail(code, sessionId.get()),
  resendEmailConfirmation: () => WebAPI._resendEmailConfirmation(sessionId.get()),
  trackRegionSwitch: (from, to) => WebAPI._trackRegionSwitch(from, to, sessionId.get()),
  trackServiceSwitch: on => WebAPI._trackServiceSwitch(on, sessionId.get()),
  reportSpeedTest: (results) => WebAPI._reportSpeedTest(results, sessionId.get()),
  requestCancel: () => WebAPI._requestCancel(sessionId.get()),
  requestRefund: () => WebAPI._requestRefund(sessionId.get()),
  redirectUrl,

  hasSession,
  isGuest,
  inService,
  signOut,

  openPayPage,
  price,
}