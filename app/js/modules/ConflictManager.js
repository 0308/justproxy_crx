import isEqual from 'lodash/isEqual'
import PersistValue from 'persist-value'

let conflicts = new PersistValue('conflicts', [])

function detect(callback) {
  chrome.proxy.settings.get({}, (data) => {
    if (data.levelOfControl === 'controlled_by_other_extensions') {
      chrome.management.getAll((exts) => {
        let ret = exts.filter((ext) => {
          return ext.enabled && ext.permissions.indexOf('proxy') > -1 && ext.id !== chrome.runtime.id
        }).map((ext) => {
          return {
            id: ext.id,
            name: ext.name,
            iconUrl: ext.icons[0].url
          }
        })
        callback(ret)
      })
    } else {
      callback([])
    }
  })
}

function resolve(callback) {
  detect((exts) => {
    exts.map((ext) => {
      chrome.management.setEnabled(ext.id, false)
    })
    conflicts.set([])
    callback && callback()
  })
}

function backgroundInit() {
  conflicts.set([])

  setInterval(() => {
    detect((exts) => {
      if (!isEqual(conflicts.get(), exts)) {
        conflicts.set(exts)
      }
    })
  }, 1000)
}

export default {
  conflicts,
  resolve,
  backgroundInit
}
