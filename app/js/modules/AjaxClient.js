const URL = require('url-parse')
const EventEmitter = require('eventemitter3')
const isEmpty = require("lodash/isEmpty")
const sortBy = require("lodash/sortBy")
const clone = require("lodash/clone")

import errors from "./errors.js";

const EVENTS = {
  DONE: 'done', //  => (success, url, ret)
}

class Client extends EventEmitter {
  constructor() {
    super()

    this._serverUrls = []
    this._timeout = undefined

    this.serverStats = {
      /*
      serverUrl: {
        success: true/false,
        updatedAt: timestamp,
      }
       */
    }
  }

  setServers(urls) {
    this._serverUrls = urls
  }

  setTimeout(timeout) {
    this._timeout = timeout
  }

  getBestServers() {
    // 优先级：较晚成功的 > 较早成功的 > 没测过的 > 较早失败的 > 较晚失败的
    let serverUrls = clone(this._serverUrls)
    return sortBy(serverUrls, url => {
      let stats = this.serverStats[url]
      if (!stats) {
        return 0
      } else if (stats.success) {
        return -stats.updatedAt
      } else {
        return stats.updatedAt
      }
    })
  }

  async ajax(settings) {
    return new Promise((resolve, reject) => {
      settings.timeout = this._timeout || 5000
      settings.complete = xhr => {
        let ret = renderJsonData(xhr)

        let success = isSuccess(ret)
        let serverUrl = getServerUrl(settings.url)
        this._recordServerStats(serverUrl, success)
        this.emit(EVENTS.DONE, success, settings.url)

        resolve(ret)
      }
      $.ajax(settings)
    })
  }

  async robustAjax(settings) {
    let serverUrls = this.getBestServers()

    if (isEmpty(serverUrls)) {
      return await this.ajax(settings)
    }

    let ret
    for (let serverUrl of serverUrls) {
      settings.url = replaceServerUrl(settings.url, serverUrl)
      ret = await this.ajax(settings)
      if (isSuccess(ret)) {
        return ret
      }
    }

    return ret
  }

  _recordServerStats(serverUrl, success) {
    if (success) {
      this.serverStats[serverUrl] = {success: true, updatedAt: new Date().getTime()}
    } else {
      this.serverStats[serverUrl] = {success: false, updatedAt: new Date().getTime()}
    }
  }
}

function isSuccess(ret) {
  switch (ret.error) {
    case errors.TIMEOUT:
    case errors.SERVER:
    case errors.PARSE:
      return false
    default:
      return true
  }
}

function renderJsonData(xhr) {
  let ret = {};
  if (xhr.statusText === 'timeout') {
    ret = {error: errors.TIMEOUT}
  } else if (xhr.status < 200 || xhr.status >= 300) {
    ret = {error: errors.SERVER}
  } else {
    try {
      ret = JSON.parse(xhr.responseText);
    } catch (e) {
      ret = {error: errors.PARSE}
    }
  }
  if (ret.error) {
    ret.message = errors.getMessage(ret);
  } else if (xhr.getResponseHeader('API-SESSION')) {
    ret['sessionId'] = xhr.getResponseHeader('API-SESSION');
  }
  return ret;
}

function getServerUrl(url) {
  let urlObj = new URL(url)
  return `${urlObj.protocol}//${urlObj.host}`
}

function replaceServerUrl(fullUrl, serverUrl) {
  let fullUrlObj = new URL(fullUrl)
  let serverObj = new URL(serverUrl)

  return fullUrlObj.set('host', serverObj.host)
                   .set('protocol', serverObj.protocol).href
}

module.exports = {
  EVENTS,
  Client,
  getServerUrl,
  replaceServerUrl,
}