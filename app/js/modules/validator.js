import locales from '../locales'

function checkEmail(email) {
  let pattern;
  if (__DEBUG) {
    pattern = /^([\w-\+]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i
  } else {
    pattern = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i
  }
  return pattern.test(email)
}

const errors = locales.en.errors.format;

function checkEmailError(email) {
  if (!email) {
    return errors.EMAIL_REQUIRED;
  } else if (!checkEmail(email)) {
    return errors.EMAIL_INVALID;
  }
  return null
}

function checkResetCodeError(code) {
  if (!code) {
    return errors.RESET_CODE_REQUIRED
  }
}

function checkConfirmCodeError(code) {
  if (!code) {
    return errors.CONFIRM_CODE_REQUIRED
  }
}

function checkPasswordError(password) {
  if (!password) {
    return errors.PASSWORD_REQUIRED;
  } else if (password.length < 6) {
    return errors.PASSWORD_MIN_LENGTH
  }
  return null
}


export default {
  checkEmail,
  checkEmailError,
  checkPasswordError,
  checkResetCodeError,
  checkConfirmCodeError
}