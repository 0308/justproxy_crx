import PersistValue from 'persist-value'

const SSL = true;
const HOST = 'www.justproxy.io'
const FALLBACK_HOSTS = ['a.justproxy.io', 'b.justproxy.io', 'oaiew.info', 'd1vl807mep8auz.cloudfront.net']
const PLAY_STORE_URL = 'https://play.google.com/store/apps/details?id=c.justproxy'


let showRateContent = new PersistValue('showRateContent', 'not set');
if (showRateContent.get() === 'not set') showRateContent.set('wait');

let showNewsAndroid = new PersistValue('showNewsAndroid', 'not set');
if (showNewsAndroid.get() === 'not set') showNewsAndroid.set('yes');

let launchCountAfterPay = new PersistValue('launchCountAfterPay', 0);

let step = new PersistValue('step', ''); //
const STEP_CONFIRM = 'confirm';
const STEP_RESET = 'reset';

let resetEmail = new PersistValue('resetEmail', '');

export default {
  SSL,
  HOST,
  FALLBACK_HOSTS,
  PLAY_STORE_URL,

  showRateContent,
  showNewsAndroid,
  launchCountAfterPay,

  step,
  STEP_CONFIRM,
  STEP_RESET,

  resetEmail,
}