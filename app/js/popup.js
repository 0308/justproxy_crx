import Vue from 'vue'
import VueRouter from 'vue-router'
import VueI18n from 'vue-i18n'

import locales from './locales'
import settings from './settings'
import User from './modules/User'
import ServicePage from './components/service/ServicePage.vue'
import RegisterView from './components/account/RegisterView.vue'
import LoginView from './components/account/LoginView.vue'
import ResetPasswordView from './components/account/ResetPasswordView.vue'
import ConfirmEmailView from './components/account/ConfirmEmailView.vue'
import BuyPageBasic from './components/buy/BuyPageBasic.vue'

Vue.use(VueI18n);
Vue.use(VueRouter);


Vue.config.debug = __DEBUG;

let defaultLocal = 'en';
let userLang = navigator.language || navigator.userLanguage;
if (userLang && userLang.slice(0, 2) === 'ru' || localStorage.getItem('lang') === 'ru') {
  defaultLocal = 'ru';
}

const i18n = new VueI18n({
  locale: defaultLocal,
  fallbackLocale: 'en',
  messages: locales,
})

const routes = [
  {path: '/service', component: ServicePage},
  {path: '/register', component: RegisterView},
  {path: '/login', component: LoginView},
  {path: '/reset-password', component: ResetPasswordView},
  {path: '/confirm-email', component: ConfirmEmailView},
  {path: '/buy', component: BuyPageBasic},
  {
    path: '/', redirect: to => {
      switch (settings.step.get()) {
        case settings.STEP_RESET:
          return {path: '/reset-password', query: {step: 2, email: settings.resetEmail.get()}}

        case settings.STEP_CONFIRM:
          return '/confirm-email'

        default:
          if (!User.hasSession()) {
            return '/register'
          } else if (
            // 没有confirm 且 不是付费用户和永久免费用户的，需要验证邮箱
            User.profilePV.get('confirmed') === false &&
            ['ACTIVE', 'EARLY_ADOPTER', 'CANCELLED'].indexOf(User.profilePV.get('status')) === -1
          ) {
            return '/confirm-email'
          } else {
            return '/service'
          }
      }
    }
  },
]

const router = new VueRouter({routes})

const app = new Vue({
  router,
  i18n,
}).$mount(('#app'))


// if (__DEBUG) {
//   router.go('/buy')
// }


User.pullProfile();